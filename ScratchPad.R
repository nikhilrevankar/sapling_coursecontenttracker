test_course <- filter(BPS_course_results, course_id == 91902 & status == "Deleted")
test_course$assessment_item_id %in% BPS_template_items$assessment_item_id
test_course$assessment_item_id %in% BPS_template_items_deleted$assessment_item_id
filter(BPS_template_items, assessment_item_id == 237518)
filter(BPS_course_results, assessment_item_id == 144693, course_id == 91902)

View(PSLS_course_results %>% 
  group_by(course_id, status) %>% 
  summarize(total = n()) %>% 
  filter(status == "Added" | status == "Deleted"))

course_91902 <- BPS_course_results %>%
  filter(course_id == 91902, status == "Added") %>%
  group_by(assessment_activity_id) %>%
  summarize(total = n())

View(BPS_course_results %>% 
  group_by(course_id,assessment_item_id) %>%
  summarize(total = n())%>%
  filter(course_id == 91902))

spec_course <- daily_report %>% 
  filter(course_id == 91902) %>% 
  mutate(in_template = assessment_item_id %in% BPS_template_items$assessment_item_id,
         status = case_when(!in_template & is.na(assessment_activity_item_mulligan) ~ "Added", 
                            !in_template & !is.na(assessment_activity_item_mulligan) ~ "Not in Template or Course",
                            in_template & is.na(assessment_activity_item_mulligan) ~ "Kept",
                            in_template & !is.na(assessment_activity_item_mulligan) & 
                              !assessment_item_id %in% BPS_template_items_deleted$assessment_item_id  ~ "Deleted",
                            in_template & !is.na(assessment_activity_item_mulligan) & 
                              assessment_item_id %in% BPS_template_items_deleted$assessment_item_id ~ 
                              "At Least One DV in Template/Not in Course")) %>%
  select(course_id, assessment_item_id, assessment_activity_id, assessment_activity_item_mulligan, in_template, status) %>%
  distinct(course_id, assessment_item_id, in_template, status, .keep_all = T)  
  kept_ids <- spec_course %>%
    filter(status == "Kept") %>%
    pull(assessment_item_id)
  spec_course <- spec_course %>%
    filter(status == "Kept" | !assessment_item_id %in% kept_ids)
spec_course

spec_course_added <- spec_course %>% 
  filter(status == "Added")
unique(spec_course_added$assessment_item_id)

n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<-n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 1 (Kept and Deleted) - 329 items
spec_course <- spec_course %>%
  filter (status == "Kept" | status == "Deleted")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 2 (Kept and Added) - NONE
spec_course <- spec_course %>%
  filter (status == "Kept" | status == "Added")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 3  (Kept and Not in Template or Course) - NONE
spec_course <- spec_course %>%
  filter (status == "Kept" | status == "Not in Template or Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 4  (Kept and At Least One DV in Template/Not in Course) - 4
spec_course <- spec_course %>%
  filter (status == "Kept" | status == "At Least One DV in Template/Not in Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

# Case 5 - Deleted and Added - NONE
spec_course <- spec_course %>%
  filter (status == "Deleted" | status == "Added")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

# Case 6 - Deleted and Not in Template or Course - NONE
spec_course <- spec_course %>%
  filter (status == "Deleted" | status == "Not in Template or Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

# Case 7 - Deleted and At Least One DV in Template/Not in Course - NONE
spec_course <- spec_course %>%
  filter (status == "Deleted" | status == "At Least One DV in Template/Not in Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 8 - Added and Not in Course/Template
spec_course <- spec_course %>%
  filter (status == "Added" | status == "Not in Template or Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 8 - Added and At Least One DV in Template
spec_course <- spec_course %>%
  filter (status == "Added" | status == "At Least One DV in Template/Not in Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

#Case 9 - Not in Template or Course and At Least One DV in Template
spec_course <- spec_course %>%
  filter (status == "Not in Template or Course" | status == "At Least One DV in Template/Not in Course")
n_occur <- data.frame(table(spec_course$assessment_item_id))
duplicated_items<- n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)

course_91902 <- filter(BPS_course_results, course_id == 91902, status == "Added")
n_occur_91902 <- data.frame(table(course_91902$assessment_item_id))

#***********************************************************************************************************************
#Viewing duplicate items in a table
n_occur <- data.frame(table(spec_course$assessment_item_id))
n_occur[n_occur$Freq > 1,]
duplicates<- spec_course[spec_course$assessment_item_id %in% n_occur$Var1[n_occur$Freq > 1],]
duplicates <- arrange(duplicates,assessment_item_id)


# kept_ids <- spec_course %>%
#   filter(status == "Kept") %>% 
#   pull(assessment_item_id)
# spec_course <- spec_course %>% 
#   filter(status == "Kept" | !assessment_item_id %in% kept_ids)
# spec_course


#Version 1 of the formula
function_BPS_course_analyzer <- function(cid) {
  spec_course <- daily_report %>% 
    filter(course_id == cid) %>% 
    mutate(in_template = assessment_item_id %in% BPS_template_items$assessment_item_id,
           status = case_when(!in_template & is.na(assessment_activity_item_mulligan) ~ "Added", 
                              !in_template & !is.na(assessment_activity_item_mulligan) ~ "Not in Template or Course",
                              in_template & is.na(assessment_activity_item_mulligan) ~ "Kept",
                              in_template & !is.na(assessment_activity_item_mulligan) & 
                                !assessment_item_id %in% BPS_template_items_deleted$assessment_item_id  ~ "Deleted",
                              in_template & !is.na(assessment_activity_item_mulligan) & 
                                assessment_item_id %in% BPS_template_items_deleted$assessment_item_id ~ 
                                "At Least One DV in Template/Not in Course")) %>%
    select(course_id, assessment_item_id, assessment_activity_id, assessment_activity_item_mulligan, in_template, status)%>%
  spec_course
}

#Version 2 of the formula
function_BPS_course_analyzer <- function(cid) {
  spec_course <- daily_report %>% 
    filter(course_id == cid) %>% 
    mutate(in_template = assessment_item_id %in% BPS_template_items$assessment_item_id,
           status = case_when(!in_template & is.na(assessment_activity_item_mulligan) ~ "Added", 
                              !in_template & !is.na(assessment_activity_item_mulligan) ~ "Not in Template or Course",
                              in_template & is.na(assessment_activity_item_mulligan) ~ "Kept",
                              in_template & !is.na(assessment_activity_item_mulligan) & 
                                !assessment_item_id %in% BPS_template_items_deleted$assessment_item_id  ~ "Deleted",
                              in_template & !is.na(assessment_activity_item_mulligan) & 
                                assessment_item_id %in% BPS_template_items_deleted$assessment_item_id ~ 
                                "At Least One DV in Template/Not in Course")) %>%
    select(course_id, assessment_item_id, assessment_activity_id, assessment_activity_item_mulligan, in_template, status) %>%
    distinct(course_id, assessment_item_id, in_template, status, .keep_all = T)
  kept_ids <- spec_course %>%
    filter(status == "Kept") %>% 
    pull(assessment_item_id)
  spec_course <- spec_course %>% 
    filter(status == "Kept" | !assessment_item_id %in% kept_ids)
  spec_course
}
  
library(dplyr)

# distinct
df %>% 
  distinct(v1, v2, v3, .keep_all = T)

# non-distinct only
df %>% 
  group_by(v1, v2, v3) %>% 
  filter(n() > 1)

# exclude any non-distinct
df %>% 
  group_by(v1, v2, v3) %>% 
  filter(n() == 1)