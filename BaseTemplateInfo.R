library(dplyr)
library(MMRsaplingcourses)
library(MMRassessment)

# Obtain base template information -------------------------
df <- data.frame(course_id = c(87084, 87085))

template_assignments <- MMRsaplingcourses::get_activities(df) %>%
focus_activities(content_type = "Assessment") 

#Get BPS8e template Assessment along with question IDs

BPS_template_questions <- filter(template_assignments, course_id == 87084) %>%
  rename(assessment_activity_id = Assessment_id) %>%
  MMRassessment::get_assessment_activity_items(server ="ushe")

#Get PSLS4e template Assessment along with question IDs

PSLS_template_questions <- filter(template_assignments, course_id == 87085) %>%
  rename(assessment_activity_id = Assessment_id) %>%
  MMRassessment::get_assessment_activity_items(server ="ushe")